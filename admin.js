
const admin = require('firebase-admin')
const config = require("config")
const serviceAccount= config.get("FIREBASE")
const fireBaseURl = config.get("FIREBASEURL")


admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: fireBaseURl
});

module.exports = admin
