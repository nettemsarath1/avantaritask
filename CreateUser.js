
const admin = require('./admin')

const CreateUser = async(req, res,next)=>{
    const {
        email,
        phoneNumber,
        password,
        firstName,
        lastName
      } = req.body;
    try {
        const user = await admin.auth().createUser({
            email,
            phoneNumber,
            password,
            displayName: `${firstName} ${lastName}`
        });

        next()
    
    } catch (error) {
        console.log('Error Man>>>>>', error)
        res.send(error)
    }
}

module.exports = {
    CreateUser 
}