
require('dotenv').config();

const express = require('express')
const mongodb = require('mongodb')
const ObjectID = require('mongodb').ObjectID ;
const bodyparser = require("body-parser");
const cors = require('cors')
const config = require('config')
const AWSKEYS = config.get("AWS")
const DATABASE = config.get("DATABASE")

const aws = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');
const {CreateUser} = require('./CreateUser')
const { checkIfAuthenticated } = require('./auth')

const app = express()

aws.config.update({
  secretAccessKey: AWSKEYS.SECRETKEY ,
  accessKeyId: AWSKEYS.ACCESSKEY ,
  region: AWSKEYS.REGION
});

const s3 = new aws.S3();

const port = process.env.PORT || 8000 ;
app.use(bodyparser.json());
app.use( cors() )

const connection_string = DATABASE.URL ;

const DbName = DATABASE.DBNAME
let db ;
const connectdb = () => {
    mongodb.MongoClient.connect(
      connection_string, { useUnifiedTopology: true },
      (err, database)=> {
        if (err) {
          console.log(err);
          process.exit(1);
        }
        console.log('Connecting to DB')
        // Save database object from the callback for reuse.
        db = database.db(DbName);
        console.log("Chat Bot Database:", db.databaseName );
       // app.emit('ready',db)
      }
    );

};
connectdb() ;

var upload = multer({
  storage: multerS3({
      s3: s3,
      bucket: 'sarathpublic',
      key: function (req, file, cb) {
          console.log(file);
          cb(null, file.originalname); //use Date.now() for unique file keys
      }
  })
});

app.on('ready',(db)=>{

  app.get('/',(req, res)=>{
    res.json({
      message : "HII"
    })
  }) 
  
  app.post('/signUp', CreateUser, async (req, res)=>{
    try {
      let userDetails = {
        email : req.body.email,
        phoneNumber : req.body.phoneNumber,
        rating:1
      }
      await db.collection('users').insertOne(userDetails)
      return res.json({
        success : true,
        message : "Registration Successfull"
      })
    } catch (error) {
      return res.json({
        success : false,
        message : "Failed to save to database"
      })
    }
  } )
  
  
  app.get('/message', checkIfAuthenticated ,async(req, res)=>{
    return res.json({
      success: true ,
      message : "Greetings From the Backend Server"
    })
  })

  app.post('/upload', checkIfAuthenticated, upload.single('avatar') ,async( req, res )=>{
    try {
      console.log('HEY Man...')
      let email = req.body.email ;

      await db.collection('userfiles').updateOne( {email:email},{ $push:{ filesList: req.file } },{ upsert:true } )
      return res.json({
        success : true,
        message : "File Upload success"
      })
    } catch (error) {
      console.log('error:', error)
      return res.json({
        success : false,
        error: error.message
      })
    }
  } )
  
  app.get('/upload/:email', checkIfAuthenticated, async(req, res)=>{
    try {
      let UploadedFiles = await db.collection('userfiles').find({email:req.params.email}).toArray()
      return res.json({
        success : true,
        UploadedFiles : UploadedFiles[0].filesList
      })
    } catch (error) {
      return res.json({
        error: error.message
      })
    }
  })
  
  app.post('/rating', checkIfAuthenticated, async(req, res)=>{
    try {
      console.log('give Rating..')
      let rating = req.body.rating ;
      let email = req.body.email ;
      await db.collection("users").updateOne({ email: email },{ $set:{ rating: rating } })
      return res.json({
        success : true,
        message:'Rating Submited Sucessfully'
      })
    } catch (error) {
      return res.json({
        success : false,
        message : error.message
      })
    }
  } )

  app.get('/rating/:email', checkIfAuthenticated, async(req, res)=>{
    try {
      let email = req.params.email ;
      let RatedUser = await db.collection("users").find({ email: email }).toArray()
      return res.json({
        success : true,
        userRating : RatedUser[0]
      })
    } catch (error) {
      return res.json({
        success : false,
        message : error.message
      })
    }
  } )

})


process.on('uncaughtException', function (err) {
  console.log(err);
  process.exit(1)
})

app.listen(port, () =>{
  console.log(`Example app listening on port ${port}!`)
} )
